## What
POC on protoc java code gen plugin using PluginPlugin and [insertion points](https://developers.google.com/protocol-buffers/docs/reference/cpp/google.protobuf.compiler.plugin.pb) 
provided by protoc.

## References
1. https://stackoverflow.com/questions/50806894/how-can-i-add-my-own-code-to-java-generated-classes-from-proto-file/58044855#58044855
1. https://expobrain.net/2015/09/13/create-a-plugin-for-google-protocol-buffer/
1. Custom annotations : https://giorgio.azzinna.ro/2017/07/extending-protobuf-custom-options/

## Note 

1. Compile protos without plugin: To generate the Options proto and dependent protos first. This is needed to register options into ExtensionsRegistry 
and also to get access to the descriptor of custom types

   Without plugin :

    ```
    /home/chockali/IdeaProjects/custom-protoc-java > protoc --proto_path=src/main/proto/ --java_out=src/main/java/ \
    src/main/proto/annotations/trinity_options.proto src/main/proto/many* src/main/proto/multiple* src/main/proto/simple_hello_world.proto src/main/proto/single* 
    ```

1. Run `./gradlew installDist`. This will make sure the executable is installed at `build/install/custom-protoc-java/bin/custom-protoc-java`

1. Now compile the protos With plugin :

    ```
    /home/chockali/IdeaProjects/custom-protoc-java > protoc --proto_path=src/main/proto/ --java_out=src/main/java/ \
    --plugin=protoc-gen-custom=build/install/custom-protoc-java/bin/custom-protoc-java  \
    --custom_out=src/main/java \
    src/main/proto/annotations/trinity_options.proto src/main/proto/many* src/main/proto/multiple* src/main/proto/simple_hello_world.proto src/main/proto/single*
    ```
1. Everything under `com.foo` package is auto-generated. `com.bar.codegen` is the actual code used in plugin.

## Gradle plugin integration

Code is available in the branch : `gradle-plugin-integration`

Concept is all same.  Instead of using the command lines above, we rely on the plugin to do the custom code-gen for us. Hence the order of task is important

`parent-proto` is the module that encloses custom plugin code AND also all proto types used by the plugin itself - LocalDate proto, TimestampWrapper proto and the options itself.

`trinity-proto` is the module that depends on the parent-proto both for protos and also for the custom plugin executable.  Notice that it is important to mention the same output directory as the java plugin for the custom plugin too - because custom plugin reads the output of java plugin and writes to the same output directory.

