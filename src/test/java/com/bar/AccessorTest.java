package com.bar;

import com.foo.javaPackage.*;
import com.foo.protoPackage.*;
import com.google.protobuf.StringValue;
import com.google.protobuf.Timestamp;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class AccessorTest {

    @Test
    public void testSimpleProto() {
        SimpleHelloWorld.Hello hello = SimpleHelloWorld.Hello.newBuilder().setGreetings("Hello").build();
        assertEquals("Hello", hello.getGreetings());
    }

    @Test
    public void testSingleMessageWithProtoPackage() {
        String fxStr = "90.018";
        SingleWithProtoPackage.FxRate fxRate = SingleWithProtoPackage.FxRate.newBuilder()
                .setEodFx(StringValue.newBuilder().setValue(fxStr).build()).build();
        assertEquals(fxRate.getEodFx().getValue(), fxStr);
        assertEquals(new BigDecimal(fxStr).compareTo(fxRate.getBigDecimalEodFx()), 0);

        fxRate = SingleWithProtoPackage.FxRate.newBuilder().setEodFx(StringValue.newBuilder().build()).build();
        assertNull(fxRate.getBigDecimalEodFx());

        fxRate = SingleWithProtoPackage.FxRate.newBuilder().build();
        assertNull(fxRate.getBigDecimalEodFx());
    }

    @Test
    public void testSingleMessageWithJavaPackage() {
        String priceStr = "90.018";
        SingleWithJavaPackage.Price price = SingleWithJavaPackage.Price.newBuilder().setEodPrice(priceStr).build();
        assertEquals(priceStr, price.getEodPrice());
        assertEquals(0, new BigDecimal(priceStr).compareTo(price.getBigDecimalEodPrice()));

        price = SingleWithJavaPackage.Price.newBuilder().setEodPrice("").build();
        assertNull(price.getBigDecimalEodPrice());

        price = SingleWithJavaPackage.Price.newBuilder().build();
        assertNull(price.getBigDecimalEodPrice());
    }

    @Test
    public void testSingleMessageWithJavaOuterClass() {
        String couponRateStr = "93.9837";
        BondProto.Bond bond = BondProto.Bond.newBuilder().setCouponRate(couponRateStr).build();
        assertEquals(couponRateStr, bond.getCouponRate());
        assertEquals(0, new BigDecimal(couponRateStr).compareTo(bond.getBigDecimalCouponRate()));

        bond = BondProto.Bond.newBuilder().build();
        assertNull(bond.getBigDecimalCouponRate());

        bond = BondProto.Bond.newBuilder().setCouponRate("").build();
        assertNull(bond.getBigDecimalCouponRate());
    }

    @Test
    public void testManyMessagesWithProtoPackage() {
        String unrealizedNotionalStr = "82.637";
        String realizedNotionalStr = "7.864";
        ManyMessagesWithProtoPackage.Pnl pnl = ManyMessagesWithProtoPackage.Pnl.newBuilder().setLtd(
                ManyMessagesWithProtoPackage.Ltd.newBuilder().setUnrealizedNotional(unrealizedNotionalStr).build()
        ).setQtd(
                ManyMessagesWithProtoPackage.Qtd.newBuilder().setRealizedNotional(realizedNotionalStr).build()
        ).build();

        assertEquals(unrealizedNotionalStr, pnl.getLtd().getUnrealizedNotional());
        assertEquals(0, new BigDecimal(unrealizedNotionalStr).compareTo(pnl.getLtd().getBigDecimalUnrealizedNotional()));

        assertEquals(realizedNotionalStr, pnl.getQtd().getRealizedNotional());
        assertEquals(0, new BigDecimal(realizedNotionalStr).compareTo(pnl.getQtd().getBigDecimalRealizedNotional()));

        pnl = ManyMessagesWithProtoPackage.Pnl.newBuilder().build();
        assertNull(pnl.getLtd().getBigDecimalUnrealizedNotional());
        assertNull(pnl.getQtd().getBigDecimalRealizedNotional());
    }

    @Test
    public void testManyMessagesWithJavaPackage() {
        ManyMessagesWithJavaPackage.Security security = ManyMessagesWithJavaPackage.Security.newBuilder()
                .setBirth(ManyMessagesWithJavaPackage.Birth.newBuilder().setBirthDate(
                        ManyMessagesWithJavaPackage.LocalDate.newBuilder().setMonth(3)
                                .setDayOfMonth(7)
                                .setYear(1992)
                ).build()).build();
        assertEquals(LocalDate.of(1992, 3, 7), security.getBirth().getJavaLocalDateBirth());
        assertEquals(ManyMessagesWithJavaPackage.LocalDate.newBuilder().setMonth(3)
                        .setDayOfMonth(7)
                        .setYear(1992).build(),
                security.getBirth().getBirthDate());
        assertNull(security.getDeath().getJavaLocalDateDeath());
    }

    @Test
    public void testManyMessagesWithJavaOuterClass() {
        EventDataProto.EventData eventData = EventDataProto.EventData.newBuilder().build();
        assertEquals(eventData.getEventTime().getTimestamp(), Timestamp.getDefaultInstance());
        assertEquals(eventData.getInstantPeriodStart(), Instant.ofEpochSecond(Timestamp.getDefaultInstance().getSeconds(),
                Timestamp.getDefaultInstance().getSeconds()));

        Instant now = Instant.now();
        Timestamp tsNow = Timestamp.newBuilder().setSeconds(now.getEpochSecond()).setNanos(now.getNano()).build();
        eventData = EventDataProto.EventData.newBuilder().setEventTime(
                EventDataProto.TimestampWrapper.newBuilder().setTimestamp(tsNow).build()
        ).build();
        assertEquals(now, eventData.getInstantPeriodStart());
    }

    @Test
    public void testMultipleFilesModeWithProtoPackage() {
        Instant now = Instant.now();
        Timestamp tsNow = Timestamp.newBuilder().setSeconds(now.getEpochSecond()).setNanos(now.getNano()).build();
        Sod sod = Sod.newBuilder().setStartOfTheDay(tsNow).build();
        assertEquals(now, sod.getInstantSod());

        Eod eod = Eod.newBuilder().setEndOfTheDay(tsNow).build();
        assertEquals(now, eod.getInstantEod());

        eod = Eod.newBuilder().build();
        assertEquals(Instant.ofEpochSecond(Timestamp.getDefaultInstance().getSeconds(),
                Timestamp.getDefaultInstance().getSeconds()), eod.getInstantEod());
    }

    @Test
    public void testMultipleFilesModeWithJavaPackage() {
        String gain = "7.392";
        RealizedGain realizedGain = RealizedGain.newBuilder().setGain(StringValue.newBuilder().setValue(gain).build()).build();
        assertEquals(0, realizedGain.getBigDecimalRealizedGain().compareTo(new BigDecimal(gain)));
        assertEquals(gain, realizedGain.getGain().getValue());
        assertEquals(Instant.ofEpochSecond(Timestamp.getDefaultInstance().getSeconds(),
                Timestamp.getDefaultInstance().getSeconds()), realizedGain.getInstantKt());

        Instant now = Instant.now();
        Timestamp tsNow = Timestamp.newBuilder().setSeconds(now.getEpochSecond()).setNanos(now.getNano()).build();
        UnrealizedGain unrealizedGain = UnrealizedGain.newBuilder().setKt(tsNow).build();
        assertNull(unrealizedGain.getBigDecimalUnrealizedGain());
        assertEquals(tsNow, unrealizedGain.getKt());
        assertEquals(now, unrealizedGain.getInstantKt());
    }

    @Test
    public void testMultipleFilesModeWithJavaOuterClass() {
        Instant now = Instant.now();
        Timestamp tsNow = Timestamp.newBuilder().setSeconds(now.getEpochSecond()).setNanos(now.getNano()).build();
        Trade trade = Trade.newBuilder().setTradeTime(tsNow).build();
        assertEquals(now, trade.getInstantTradeTime());
        assertEquals(tsNow, trade.getTradeTime());

        CashDividend cashDividend = CashDividend.newBuilder().build();
        assertEquals(Instant.ofEpochSecond(Timestamp.getDefaultInstance().getSeconds(),
                Timestamp.getDefaultInstance().getSeconds()), cashDividend.getInstantExDate());
    }
}
