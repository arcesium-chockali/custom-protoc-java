// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: multiple_messages_with_java_package.proto

package com.foo.javaPackage;

public interface RealizedGainOrBuilder extends
    // @@protoc_insertion_point(interface_extends:com.foo.protoPackage.RealizedGain)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>.google.protobuf.StringValue gain = 1 [(.trinity.java_accessor_options) = { ... }</code>
   * @return Whether the gain field is set.
   */
  boolean hasGain();
  /**
   * <code>.google.protobuf.StringValue gain = 1 [(.trinity.java_accessor_options) = { ... }</code>
   * @return The gain.
   */
  com.google.protobuf.StringValue getGain();
  /**
   * <code>.google.protobuf.StringValue gain = 1 [(.trinity.java_accessor_options) = { ... }</code>
   */
  com.google.protobuf.StringValueOrBuilder getGainOrBuilder();

  /**
   * <code>.google.protobuf.Timestamp kt = 2 [(.trinity.java_accessor_options) = { ... }</code>
   * @return Whether the kt field is set.
   */
  boolean hasKt();
  /**
   * <code>.google.protobuf.Timestamp kt = 2 [(.trinity.java_accessor_options) = { ... }</code>
   * @return The kt.
   */
  com.google.protobuf.Timestamp getKt();
  /**
   * <code>.google.protobuf.Timestamp kt = 2 [(.trinity.java_accessor_options) = { ... }</code>
   */
  com.google.protobuf.TimestampOrBuilder getKtOrBuilder();
}
