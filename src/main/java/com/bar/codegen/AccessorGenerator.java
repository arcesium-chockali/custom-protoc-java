package com.bar.codegen;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import com.google.common.base.Charsets;
import com.google.protobuf.DescriptorProtos;
import com.google.protobuf.compiler.PluginProtos;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface AccessorGenerator {

    String JAVA_EXTENSION = ".java";

    MustacheFactory mustacheFactory = new DefaultMustacheFactory();

    default List<PluginProtos.CodeGeneratorResponse.File> generateFiles(PluginProtos.CodeGeneratorRequest request) {
        List<DescriptorProtos.FileDescriptorProto> protoFileList = request.getProtoFileList();

        List<PluginProtos.CodeGeneratorResponse.File> responseFiles = new ArrayList<>();

        for (DescriptorProtos.FileDescriptorProto fileDescriptor : protoFileList) {

            String outerClassName = fileDescriptor.getOptions().hasJavaOuterClassname()
                    ? fileDescriptor.getOptions().getJavaOuterClassname()
                    : convertToJavaClassName(getProtoFileNameWithoutDotProtoExtension(fileDescriptor));

            String packageName = fileDescriptor.getOptions().hasJavaPackage()
                    ? fileDescriptor.getOptions().getJavaPackage()
                    : fileDescriptor.getPackage();

            boolean hasJavaMultipleFiles = fileDescriptor.getOptions().hasJavaMultipleFiles();

            for (DescriptorProtos.DescriptorProto messageTypeDescriptor : fileDescriptor.getMessageTypeList()) {
                String fileName = hasJavaMultipleFiles
                        ? determineFileName(packageName, messageTypeDescriptor.getName())
                        : determineFileName(packageName, outerClassName);
                for (DescriptorProtos.FieldDescriptorProto fieldDescriptor : messageTypeDescriptor.getFieldList()) {

                    PluginProtos.CodeGeneratorResponse.File file = handleField(fieldDescriptor, fileName,
                            messageTypeDescriptor.getName(),
                            fileDescriptor.getPackage(),
                            fileDescriptor.getName());
                    if (file != null) {
                        responseFiles.add(file);
                    }
                }
            }

        }
        return responseFiles;
    }

    PluginProtos.CodeGeneratorResponse.File handleField(DescriptorProtos.FieldDescriptorProto fieldDescriptor,
                                                        String fileName,
                                                        String messageName,
                                                        String protoPackageName,
                                                        String protoFileName);

    boolean isSupportedType(DescriptorProtos.FieldDescriptorProto fieldDescriptor);

    static String getCurrentType(DescriptorProtos.FieldDescriptorProto fieldDescriptor) {
        String currentType;
        if (fieldDescriptor.getType().equals(DescriptorProtos.FieldDescriptorProto.Type.TYPE_MESSAGE)) {
            currentType = fieldDescriptor.getTypeName();
        } else {
            currentType = fieldDescriptor.getType().name();
        }
        if (currentType.startsWith(".")) {
            currentType = currentType.substring(1);
        }
        return currentType;
    }

    static String executeTemplate(String templateResource, Map<String, String> templateContext) {
        InputStream resource = MustacheFactory.class.getClassLoader().getResourceAsStream(templateResource);
        if (resource == null) {
            throw new RuntimeException("Could not find resource " + templateResource);
        }

        InputStreamReader resourceReader = new InputStreamReader(resource, Charsets.UTF_8);
        Mustache template = mustacheFactory.compile(resourceReader, templateResource);

        return template.execute(new StringWriter(), templateContext).toString();
    }

    static String determineFileName(String packageName, String className) {
        return packageName.replace(".", "/") + "/" + className + JAVA_EXTENSION;
    }

    static String getProtoFileNameWithoutDotProtoExtension(DescriptorProtos.FileDescriptorProto fileDescriptor) {
        return fileDescriptor.getName().split("\\.")[0];
    }

    static String convertToJavaClassName(String protoFileName) {
        return capitalizeFirstLetter(underscoreToCamel(protoFileName));
    }

    static String uncapitalizeFirstLetter(String str) {
        return str.substring(0, 1).toLowerCase() + str.substring(1);
    }

    static String capitalizeFirstLetter(String str) {
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }

    static String getJavaFieldName(String fieldName) {
        String name = underscoreToCamel(fieldName);
        return uncapitalizeFirstLetter(name) + "_";
    }

    static String getJavaMethodNameForField(String fieldName) {
        String name = underscoreToCamel(fieldName);
        return "get" + capitalizeFirstLetter(name);
    }

    static String getHasAccessorForField(String fieldName) {
        String name = underscoreToCamel(fieldName);
        return "has" + capitalizeFirstLetter(name);
    }

    static String underscoreToCamel(String fieldName) {
        if (fieldName.contains("_")) {
            return Stream.of(fieldName.split("_"))
                    .map(AccessorGenerator::capitalizeFirstLetter)
                    .collect(Collectors.joining());
        } else {
            return fieldName;
        }
    }
}
