package com.bar.codegen;

import static com.bar.codegen.AccessorGenerator.*;

import com.foo.javaPackage.EventDataProto;
import com.foo.javaPackage.TrinityOptions;
import com.google.protobuf.DescriptorProtos;
import com.google.protobuf.Timestamp;
import com.google.protobuf.compiler.PluginProtos;

import java.util.List;
import java.util.Map;

public class InstantAccessorGenerator implements AccessorGenerator {

    @Override
    public PluginProtos.CodeGeneratorResponse.File handleField(DescriptorProtos.FieldDescriptorProto fieldDescriptor,
                                                               String fileName, String messageName,
                                                               String protoPackageName, String protoFileName) {
        TrinityOptions.TrinityJavaAccessorOptions trinityJavaAccessorOptions
                = fieldDescriptor.getOptions().getExtension(TrinityOptions.javaAccessorOptions);

        if (trinityJavaAccessorOptions.hasInstantAccessor()) {
            String protoFieldName = fieldDescriptor.getName();
            String instantAccessor = trinityJavaAccessorOptions.getInstantAccessor();
            String javaMethodNameForField = AccessorGenerator.getJavaMethodNameForField(protoFieldName);
            if (instantAccessor.equals(javaMethodNameForField)) {
                throw new IllegalArgumentException("Unable to process instantAccessor annotation on field " + protoFieldName
                        + " at proto package : " + protoPackageName
                        + " File : " + protoFileName
                        + ". Value passed is same as the original accessor : " + javaMethodNameForField);
            }

            String javaFieldName = AccessorGenerator.getJavaFieldName(protoFieldName);
            if (!isSupportedType(fieldDescriptor)) {
                throw new IllegalArgumentException("Unable to process instantAccessor annotation on field " + protoFieldName
                        + " at proto package : " + protoPackageName
                        + " File : " + protoFileName
                        + ". instantAccessor annotation can only be applied types : " + getSupportedTypes()
                        + " But the field is of type : " + getCurrentType(fieldDescriptor)
                );
            }

            String codeBlock = generateCodeBlock(fieldDescriptor, javaFieldName, javaMethodNameForField, instantAccessor);
            return PluginProtos.CodeGeneratorResponse.File.newBuilder()
                    .setName(fileName)
                    .setContent(codeBlock)
                    .setInsertionPoint("class_scope:" + protoPackageName + "." + messageName)
                    .build();
        }
        return null;
    }

    @Override
    public boolean isSupportedType(DescriptorProtos.FieldDescriptorProto fieldDescriptor) {
        return getSupportedTypes().contains(getCurrentType(fieldDescriptor));
    }


    public List<String> getSupportedTypes() {
        return List.of(EventDataProto.TimestampWrapper.getDescriptor().getFullName(),
                Timestamp.getDescriptor().getFullName());
    }

    private String generateCodeBlock(DescriptorProtos.FieldDescriptorProto fieldDescriptor,
                                     String javaFieldName, String javaMethodNameForField, String instantAccessor) {
        String currentType = getCurrentType(fieldDescriptor);
        if (currentType.equals(EventDataProto.TimestampWrapper.getDescriptor().getFullName())) {
            return generateCodeBlockForTimestampWrapperToInstant(javaFieldName, javaMethodNameForField, instantAccessor);
        } else if(currentType.equals(Timestamp.getDescriptor().getFullName())) {
            return generateCodeBlockForGoogleTimestampToInstant(javaFieldName, javaMethodNameForField, instantAccessor);
        }
        return "";
    }

    private String generateCodeBlockForGoogleTimestampToInstant(String javaFieldName, String javaMethodNameForField,
                                                                String instantAccessor) {
        String templateResource = "googleTimestamp-to-javaInstant.mustache";
        Map<String,String> templateContext = Map.of(
                "newInstantJavaFieldName",  "instant"+ AccessorGenerator.capitalizeFirstLetter(javaFieldName),
                "newInstantJavaAccessor", instantAccessor,
                "parentJavaAccessor", javaMethodNameForField
        );
        return AccessorGenerator.executeTemplate(templateResource, templateContext);
    }

    private String generateCodeBlockForTimestampWrapperToInstant(String javaFieldName,
                                                                       String javaMethodNameForField,
                                                                       String instantAccessor) {
        String templateResource = "trinityTimestampWrapper-to-javaInstant.mustache";
        Map<String,String> templateContext = Map.of(
                "newInstantJavaFieldName",  "instant"+ AccessorGenerator.capitalizeFirstLetter(javaFieldName),
                "newInstantJavaAccessor", instantAccessor,
                "parentJavaAccessor", javaMethodNameForField
        );
        return AccessorGenerator.executeTemplate(templateResource, templateContext);
    }
}
