package com.bar.codegen;

import com.google.common.io.ByteStreams;
import com.google.protobuf.ExtensionRegistry;
import com.google.protobuf.compiler.PluginProtos;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class TrinityCodeGen {
    public static void main(String[] args) {
        try {
            byte[] generatorRequestBytes = ByteStreams.toByteArray(System.in);
            ExtensionRegistry extensionRegistry = ExtensionRegistry.newInstance();
            extensionRegistry.add(com.foo.javaPackage.TrinityOptions.javaAccessorOptions);
            PluginProtos.CodeGeneratorRequest request = PluginProtos.CodeGeneratorRequest.parseFrom(
                    generatorRequestBytes, extensionRegistry);

            List<AccessorGenerator> generators = List.of(new BigDecimalAccessorGenerator(),
                    new LocalDateAccessorGenerator(),
                    new InstantAccessorGenerator());
            List<PluginProtos.CodeGeneratorResponse.File> files = generators.stream().map(
                    g -> g.generateFiles(request)
            ).flatMap(Collection::stream).collect(Collectors.toList());

            PluginProtos.CodeGeneratorResponse response = PluginProtos.CodeGeneratorResponse.newBuilder()
                    .addAllFile(files).build();
            response.writeTo(System.out);

        } catch (Throwable ex) {
            try {
                PluginProtos.CodeGeneratorResponse
                        .newBuilder()
                        .setError(ex.getMessage())
                        .build()
                        .writeTo(System.out);
            } catch (IOException ex2) {
                abort(ex2);
            }
            abort(ex);
        }
    }

    private static void abort(Throwable ex) {
        ex.printStackTrace(System.err);
        System.exit(1);
    }
}
