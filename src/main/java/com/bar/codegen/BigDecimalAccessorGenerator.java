package com.bar.codegen;

import com.foo.javaPackage.TrinityOptions;
import com.google.protobuf.DescriptorProtos;
import com.google.protobuf.StringValue;
import com.google.protobuf.compiler.PluginProtos;

import java.util.List;
import java.util.Map;

import static com.bar.codegen.AccessorGenerator.*;


public class BigDecimalAccessorGenerator implements AccessorGenerator {

    @Override
    public PluginProtos.CodeGeneratorResponse.File handleField(DescriptorProtos.FieldDescriptorProto fieldDescriptor,
                                                               String fileName, String messageName,
                                                               String protoPackage, String protoFileName
    ) {
        TrinityOptions.TrinityJavaAccessorOptions trinityJavaAccessorOptions
                = fieldDescriptor.getOptions().getExtension(TrinityOptions.javaAccessorOptions);
        if (trinityJavaAccessorOptions.hasBigDecimalAccessor()) {
            String protoFieldName = fieldDescriptor.getName();
            String bigDecimalAccessor = trinityJavaAccessorOptions.getBigDecimalAccessor();
            String javaMethodNameForField = getJavaMethodNameForField(protoFieldName);
            if (bigDecimalAccessor.equals(javaMethodNameForField)) {
                throw new IllegalArgumentException("Unable to process bigDecimalAccessor annotation on field " + protoFieldName
                        + " at proto package : " + protoPackage
                        + " File : " + protoFileName
                        + ". Value passed is same as the original accessor : " + javaMethodNameForField);
            }

            String javaFieldName = getJavaFieldName(protoFieldName);
            if (!isSupportedType(fieldDescriptor)) {
                throw new IllegalArgumentException("Unable to process bigDecimalAccessor annotation on field " + protoFieldName
                        + " at proto package : " + protoPackage
                        + " File : " + protoFileName
                        + ". bigDecimalAccessor annotation can only be applied types : " + getSupportedTypes()
                        + " But the field is of type : " + getCurrentType(fieldDescriptor)
                );
            }

            String codeBlock = generateCodeBlock(fieldDescriptor, javaFieldName, javaMethodNameForField, bigDecimalAccessor);
            return PluginProtos.CodeGeneratorResponse.File.newBuilder()
                    .setName(fileName)
                    .setContent(codeBlock)
                    // TODO: does this insertion point change as per the Java package name?
                    //  No. Always proto package + proto message name
                    .setInsertionPoint("class_scope:" + protoPackage + "." + messageName)
                    .build();
        }
        return null;
    }

    @Override
    public boolean isSupportedType(DescriptorProtos.FieldDescriptorProto fieldDescriptor) {
        return getSupportedTypes().contains(getCurrentType(fieldDescriptor));
    }

    public List<String> getSupportedTypes() {
        return List.of(DescriptorProtos.FieldDescriptorProto.Type.TYPE_STRING.name(),
                StringValue.getDescriptor().getFullName());
    }

    private String generateCodeBlock(DescriptorProtos.FieldDescriptorProto fieldDescriptor, String javaFieldName, String javaMethodNameForField, String bigDecimalAccessor) {
        String currentType = getCurrentType(fieldDescriptor);
        if (currentType.equals(DescriptorProtos.FieldDescriptorProto.Type.TYPE_STRING.name())) {
            return generateCodeBlockForStringToBigDecimal(javaFieldName, javaMethodNameForField, bigDecimalAccessor);
        } else if (currentType.equals(StringValue.getDescriptor().getFullName())) {
            return generateCodeBlockForStringValueToBigDecimal(javaFieldName, javaMethodNameForField, bigDecimalAccessor);
        }
        return "";
    }

    private String generateCodeBlockForStringToBigDecimal(String javaFieldName, String javaMethodNameForField, String bigDecimalAccessor) {
        String templateResource = "string-to-bigDecimal.mustache";
        Map<String, String> templateContext = Map.of(
                "newBdJavaFieldName", "bd" + capitalizeFirstLetter(javaFieldName),
                "newBdJavaAccessor", bigDecimalAccessor,
                "parentJavaAccessor", javaMethodNameForField
        );
        return executeTemplate(templateResource, templateContext);
    }

    private String generateCodeBlockForStringValueToBigDecimal(String javaFieldName, String javaMethodNameForField, String bigDecimalAccessor) {
        String templateResource = "stringValue-to-bigDecimal.mustache";
        Map<String, String> templateContext = Map.of(
                "newBdJavaFieldName", "bd" + capitalizeFirstLetter(javaFieldName),
                "newBdJavaAccessor", bigDecimalAccessor,
                "parentJavaAccessor", javaMethodNameForField
        );
        return executeTemplate(templateResource, templateContext);
    }

}
