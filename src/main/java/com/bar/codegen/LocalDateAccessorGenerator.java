package com.bar.codegen;

import com.foo.javaPackage.ManyMessagesWithJavaPackage;
import com.foo.javaPackage.TrinityOptions;
import com.google.protobuf.DescriptorProtos;
import com.google.protobuf.compiler.PluginProtos;

import java.util.List;
import java.util.Map;

public class LocalDateAccessorGenerator implements AccessorGenerator {

    @Override
    public PluginProtos.CodeGeneratorResponse.File handleField(DescriptorProtos.FieldDescriptorProto fieldDescriptor,
                                                               String fileName, String messageName,
                                                               String protoPackageName, String protoFileName) {
        TrinityOptions.TrinityJavaAccessorOptions trinityJavaAccessorOptions
                = fieldDescriptor.getOptions().getExtension(TrinityOptions.javaAccessorOptions);

        if (trinityJavaAccessorOptions.hasLocalDateAccessor()) {
            String protoFieldName = fieldDescriptor.getName();
            String localDateAccessor = trinityJavaAccessorOptions.getLocalDateAccessor();
            String javaMethodNameForField = AccessorGenerator.getJavaMethodNameForField(protoFieldName);
            String hasMethodNameForField = AccessorGenerator.getHasAccessorForField(protoFieldName);
            if (localDateAccessor.equals(javaMethodNameForField)) {
                throw new IllegalArgumentException("Unable to process localDateAccessor annotation on field " + protoFieldName
                        + " at proto package : " + protoPackageName
                        + " File : " + protoFileName
                        + ". Value passed is same as the original accessor : " + javaMethodNameForField);
            }

            String javaFieldName = AccessorGenerator.getJavaFieldName(protoFieldName);
            if (!isSupportedType(fieldDescriptor)) {
                throw new IllegalArgumentException("Unable to process localDateAccessor annotation on field " + protoFieldName
                        + " at proto package : " + protoPackageName
                        + " File : " + protoFileName
                        + ". localDateAccessor annotation can only be applied types : " + getSupportedTypes()
                        + " But the field is of type : " + AccessorGenerator.getCurrentType(fieldDescriptor)
                );
            }

            String codeBlock = generateCodeBlock(fieldDescriptor, javaFieldName, javaMethodNameForField,
                    hasMethodNameForField, localDateAccessor);
            return PluginProtos.CodeGeneratorResponse.File.newBuilder()
                    .setName(fileName)
                    .setContent(codeBlock)
                    .setInsertionPoint("class_scope:" + protoPackageName + "." + messageName)
                    .build();
        }
        return null;
    }

    @Override
    public boolean isSupportedType(DescriptorProtos.FieldDescriptorProto fieldDescriptor) {
        return getSupportedTypes().contains(AccessorGenerator.getCurrentType(fieldDescriptor));
    }

    public List<String> getSupportedTypes() {
        return List.of(ManyMessagesWithJavaPackage.LocalDate.getDescriptor().getFullName());
    }

    private String generateCodeBlock(DescriptorProtos.FieldDescriptorProto fieldDescriptor, String javaFieldName,
                                     String javaMethodNameForField,
                                     String hasMethodNameForField,
                                     String localDateAccessor) {
        String currentType = AccessorGenerator.getCurrentType(fieldDescriptor);
        if (currentType.equals(ManyMessagesWithJavaPackage.LocalDate.getDescriptor().getFullName())) {
            return generateCodeBlockForTrinityLocalDateToJavaLocalDate(javaFieldName, javaMethodNameForField,
                    hasMethodNameForField, localDateAccessor);
        }
        return "";
    }

    private String generateCodeBlockForTrinityLocalDateToJavaLocalDate(String javaFieldName,
                                                                       String javaMethodNameForField,
                                                                       String hasMethodNameForField,
                                                                       String localDateAccessor) {
        String templateResource = "trinityLocalDate-to-javaLocalDate.mustache";
        Map<String, String> templateContext = Map.of(
                "newLdJavaFieldName", "ld" + AccessorGenerator.capitalizeFirstLetter(javaFieldName),
                "newLdJavaAccessor", localDateAccessor,
                "parentJavaAccessor", javaMethodNameForField,
                "parentJavaHasAccessor", hasMethodNameForField
        );
        return AccessorGenerator.executeTemplate(templateResource, templateContext);
    }
}
